//
//  SFAPIClient.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFAPIClient.h"

@implementation SFAPIClient

+ (id)sharedClient {
    static SFAPIClient *sharedAPIClient = nil;
    @synchronized(self) {
        if (sharedAPIClient == nil)
            sharedAPIClient = [[self alloc] init];
    }
    return sharedAPIClient;
}

- (void)fetchDataFromPath:(NSString *)path cached:(BOOL)cached completion:(void (^)(NSData *data, NSError *error))completionHandler {
    NSURLRequestCachePolicy cashPolicy = cached ? NSURLRequestReturnCacheDataElseLoad : NSURLRequestReloadIgnoringLocalCacheData;
    NSString *encodedPath = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:encodedPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:cashPolicy timeoutInterval:10];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (completionHandler == nil) return;
        
        if (error) {
            completionHandler(nil, error);
            return;
        }
        completionHandler(data, nil);
    }] resume];
}

@end
