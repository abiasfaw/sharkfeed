//
//  SFAPIClient.h
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SFAPIClient : NSObject

+ (id)sharedClient;

- (void)fetchDataFromPath:(NSString *)path cached:(BOOL)cached completion:(void (^)(NSData *data, NSError *error))completionHandler;

@end
