//
//  SFPhotoDetailsViewController.h
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFPhoto.h"

@interface SFPhotoDetailsViewController : UIViewController

@property (strong, nonatomic) SFPhoto *photo;

@end
