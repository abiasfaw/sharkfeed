//
//  SFLounchViewController.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/6/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFLaunchViewController.h"

@interface SFLaunchViewController ()

@property (weak, nonatomic) IBOutlet UIView *swipeToFeedView;

@end

@implementation SFLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToFeedAction:)];
    [self.swipeToFeedView addGestureRecognizer:swipeGesture];
}

#pragma mark - Actions

- (void)swipeToFeedAction:(UISwipeGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateEnded) {
        if(recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
            [self performSegueWithIdentifier:@"ShowSFPhotoGridVeiwController" sender:self];
        }
    }
}

@end
