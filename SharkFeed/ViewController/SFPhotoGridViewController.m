//
//  SFPhotoGridViewController.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/6/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFPhotoGridViewController.h"
#import "SFPhotoCollectionViewCell.h"
#import "SFPhotoDetailsViewController.h"
#import "SFConstants.h"
#import "SFAPIClient.h"
#import "SFPhoto.h"

static int const EDGE_INSET = 10.0;
static CGFloat const MIN_SPACING = 5.0;
static CGFloat const CELL_DIMENSION = 115.0;
static NSString *const SEARCH_TEXT = @"shark";
static NSString *const REFRESH_CONTROL_CAPTION = @"Pull to refresh sharks";
static int const PHOTO_PREFETCH_MARGIN = 40;
static int const IMAGE_PREFETCH_MARGIN = 10;
static int const IMAGE_PREFETCH_COUNT = 20;

@interface SFPhotoGridViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) IBOutlet UICollectionView *gridView;
@property (nonatomic) NSMutableArray *photos;
@property (nonatomic) NSCache *imagesCache;
@property (nonatomic) int pageNumber;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger nextImagePrefetchStartIndex;

@end

@implementation SFPhotoGridViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _photos = [[NSMutableArray alloc] init];
    _pageNumber = 1;
    _imagesCache = [[NSCache alloc]init];
    _nextImagePrefetchStartIndex = 0;
    
    [self.gridView registerClass:[SFPhotoCollectionViewCell class] forCellWithReuseIdentifier:[SFPhotoCollectionViewCell identifier]];
    [self addRefreshControl];
    [self fetchPhotoObjectsWithSearchText:SEARCH_TEXT onPage:self.pageNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Data

- (void)fetchPhotoObjectsWithSearchText:(NSString*)searchText onPage:(int)pageNumber {
    
    
    NSString *path = [NSString stringWithFormat:@"%@?method=%@&api_key=%@&text=%@&format=json&nojsoncallback=1&page=%d&extras=url_t,url_c,url_l,url_o",
                      FlickrAPIRoot, FlickrAPISearchMethod, FlickrAPIKey, searchText, pageNumber];
   
    __weak typeof(self) weakSelf = self;
    
    [[SFAPIClient sharedClient] fetchDataFromPath:path cached:NO completion:^(NSData *data, NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
        NSError *DataError = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&DataError];
        if (DataError) {
            NSLog(@"JSON data error: %@", DataError.localizedDescription);
        }
        else {
            if(pageNumber == 1) {
                weakSelf.nextImagePrefetchStartIndex = 0;
                [weakSelf.photos removeAllObjects];
            }
            
            weakSelf.pageNumber = pageNumber + 1;
            NSArray *photoDictionriesArray = [[dictionary objectForKey:@"photos"] objectForKey:@"photo"];
            for(NSDictionary *photoInfo in photoDictionriesArray) {
                SFPhoto *photo = [[SFPhoto alloc] initWithDictionary:photoInfo];
                if(photo) {
                    [weakSelf.photos addObject:photo];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(pageNumber == 1) {
                    [weakSelf prefetchImagesForPhotosStartingAtIndex:weakSelf.nextImagePrefetchStartIndex];
                    weakSelf.nextImagePrefetchStartIndex += IMAGE_PREFETCH_MARGIN;
                }
                
                if(weakSelf.refreshControl.isRefreshing) {
                    [weakSelf.refreshControl endRefreshing];
                }
                
                [weakSelf.gridView reloadData];
            });
        }
    }];
}

- (void)downloadImageWithURL:(NSString*)url {
    __weak typeof(self) weakSelf = self;
    
    [[SFAPIClient sharedClient] fetchDataFromPath:url cached:YES completion:^(NSData *data, NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        UIImage *image = [[UIImage alloc] initWithData:data];
        [_imagesCache setObject:image forKey:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.gridView reloadData];
        });
    }];
}

- (void)prefetchImagesForPhotosStartingAtIndex:(NSInteger)startIndex {
    NSArray *photosCopy = [_photos copy];
    for(NSInteger index = startIndex; index < photosCopy.count && index < startIndex + 20; index++) {
        SFPhoto *photo = [photosCopy objectAtIndex:index];
        NSString *url = nil;
        
        if(photo.thumbnailURL) {
            url = photo.thumbnailURL;
        }
        else if(photo.mediumSizeURL) {
            url = photo.mediumSizeURL;
        }
        else {
            continue;
        }
        
        if(url && [_imagesCache objectForKey:url] == nil) {
            [self downloadImageWithURL:url];
        }
    }
    
}

#pragma mark - UICollectionView data source

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    SFPhotoCollectionViewCell *cell = [self.gridView dequeueReusableCellWithReuseIdentifier:[SFPhotoCollectionViewCell identifier] forIndexPath:indexPath];
    
    SFPhoto *photo = [_photos objectAtIndex:indexPath.row];
    
    NSString *url = nil;
    if(photo.thumbnailURL) {
        url = photo.thumbnailURL;
    }
    else if(photo.mediumSizeURL) {
        url = photo.mediumSizeURL;
    }
    
    if(url) {
        UIImage *photoImage = [_imagesCache objectForKey:url];
        if (photoImage) {
            cell.imageView.image = photoImage;
        }
        else {
            [self downloadImageWithURL:url];
        }
    }
    
    if(_nextImagePrefetchStartIndex - indexPath.row > IMAGE_PREFETCH_MARGIN) {
        [self prefetchImagesForPhotosStartingAtIndex:_nextImagePrefetchStartIndex];
        _nextImagePrefetchStartIndex += IMAGE_PREFETCH_COUNT;
    }
    
    if(_photos.count - indexPath.row < PHOTO_PREFETCH_MARGIN) {
        [self fetchPhotoObjectsWithSearchText:SEARCH_TEXT onPage:_pageNumber];
    }
    
    cell.imageView.backgroundColor = UIColor.lightGrayColor;
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photos.count;
}

#pragma mark - UICollectionView delegate

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CELL_DIMENSION, CELL_DIMENSION);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return MIN_SPACING;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(EDGE_INSET, EDGE_INSET, EDGE_INSET, EDGE_INSET);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return MIN_SPACING;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SFPhoto *photo = [_photos objectAtIndex:indexPath.row];
    NSString *imageURL = [photo getLargeSizeOrSmallerImageURL];
    if(imageURL) {
        [self downloadImageWithURL:imageURL];
    }
    
    SFPhotoDetailsViewController *photoDetailsVC = (SFPhotoDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SFPhotoDetailsViewController"];;
    
    photoDetailsVC.photo = photo;
    [self presentViewController:photoDetailsVC animated:YES completion:nil];
}

#pragma mark - Helpers

- (void)refreshGrid {
    _pageNumber = 1;
    [self fetchPhotoObjectsWithSearchText:SEARCH_TEXT onPage:_pageNumber];
}

- (void)addRefreshControl {
    _refreshControl = [[UIRefreshControl alloc]init];
    [_refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:REFRESH_CONTROL_CAPTION
                                                                        attributes:@{NSForegroundColorAttributeName: UIColor.lightGrayColor}]];
    [_refreshControl addTarget:self action:@selector(refreshGrid) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        self.gridView.refreshControl = _refreshControl;
    } else {
        [self.gridView addSubview:_refreshControl];
    }
}

@end
