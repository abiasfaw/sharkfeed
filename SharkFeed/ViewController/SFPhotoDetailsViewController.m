//
//  SFPhotoDetailsViewController.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFPhotoDetailsViewController.h"
#import "SFAPIClient.h"

@interface SFPhotoDetailsViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *downloadOriginalButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation SFPhotoDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.errorLabel.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    if(self.photo) {
        [self setUpWithPhoto];
    }
    
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOverlayUI:)];
    singleTapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGesture];
    
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZoom:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
    
    [singleTapGesture requireGestureRecognizerToFail:doubleTapGesture];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateUI];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         self.imageView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
         self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height);
         self.scrollView.contentOffset = CGPointMake(0, 0);
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         
     }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - Helpers

- (void)setUpWithPhoto {
    self.titleLabel.text = _photo.title;
    
    if(!_photo.originalSizeURL) {
        self.downloadOriginalButton.hidden = YES;
    }
    
    NSString *imageURL = [_photo getLargeSizeOrSmallerImageURL];
    if(imageURL) {
        [self downloadImageWithURL:imageURL savesToCameraRoll:NO];
    }
}

- (void)updateImage:(UIImage *)image {
    if(image) {
        self.errorLabel.hidden = YES;

        self.imageView.image = image;
        self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        
        self.scrollView.minimumZoomScale = [self getImageFittingScale];
        self.scrollView.maximumZoomScale = 1;
        self.scrollView.zoomScale = [self getImageFittingScale];
    }
}

- (void)updateUI {
    if(self.imageView.image) {
        self.imageView.frame = CGRectMake(0, 0, self.imageView.image.size.width, self.imageView.image.size.height);
        
        self.scrollView.minimumZoomScale = [self getImageFittingScale];
        self.scrollView.maximumZoomScale = 1;
        self.scrollView.zoomScale = [self getImageFittingScale];
    }
}

- (CGFloat)getImageFittingScale {
    CGFloat zoomScale = 1;
    
    if(self.imageView.image) {
        zoomScale = MIN(self.scrollView.frame.size.width /self.imageView.image.size.width, self.scrollView.frame.size.height / self.imageView.image.size.height);;
    }
    return zoomScale;
}

- (void)downloadImageWithURL:(NSString*)url savesToCameraRoll:(BOOL)saves {
    [self.activityIndicator startAnimating];
    __weak typeof(self) weakSelf = self;
    
    [[SFAPIClient sharedClient] fetchDataFromPath:url cached:YES completion:^(NSData *data, NSError *error) {
        if (error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(!self.imageView.image) {
                    self.errorLabel.text = @"Error downloading photo.";
                    self.errorLabel.hidden = NO;
                }
                [self.activityIndicator stopAnimating];
                NSLog(@"%@", error.localizedDescription);
            });
            
            return;
        }
        
        UIImage *image = [[UIImage alloc] initWithData:data];
        if (saves) {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            [weakSelf updateImage: image];
        });
    }];
}

#pragma mark - Gesture Actions

- (void)toggleZoom:(UITapGestureRecognizer *)recognizer {
    CGFloat zoomScale = self.scrollView.zoomScale > self.scrollView.minimumZoomScale? self.scrollView.minimumZoomScale : self.scrollView.maximumZoomScale;
    [self.scrollView setZoomScale:zoomScale animated:YES];
    self.topBar.hidden = YES;
    self.bottomBar.hidden = YES;
    self.titleLabel.hidden = YES;
}

- (void)toggleOverlayUI:(UITapGestureRecognizer *)recognizer {
    self.titleLabel.hidden = !self.titleLabel.hidden;
    self.bottomBar.hidden = !self.bottomBar.hidden;
    self.topBar.hidden = !self.topBar.hidden;
}

#pragma mark - Button Actions

- (IBAction)closeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)openInAppButtonAction:(id)sender {
    NSString *flickrUrlString = [NSString stringWithFormat:@"https://www.flickr.com/photos/%@/%@", _photo.ownerId, _photo.photoId];
    NSURL *flickrUrl = [[NSURL alloc] initWithString: flickrUrlString];
    if ([[UIApplication sharedApplication] canOpenURL:flickrUrl]) {
        [[UIApplication sharedApplication] openURL:flickrUrl];
    } else {
        NSLog(@"Unable to open Flickr app at: %@", flickrUrl);
    }
}

- (IBAction)downloadButtonAction:(id)sender {
    [self downloadImageWithURL:_photo.originalSizeURL savesToCameraRoll:YES];
}

#pragma mark - UIScrollViewDelegate

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat offsetX = MAX((self.scrollView.bounds.size.width - self.scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((self.scrollView.bounds.size.height - self.scrollView.contentSize.height) * 0.5, 0.0);
    
    self.imageView.center = CGPointMake(self.scrollView.contentSize.width * 0.5 + offsetX,
                                        self.scrollView.contentSize.height * 0.5 + offsetY);
}

@end
