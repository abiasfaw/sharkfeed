//
//  AppDelegate.h
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/6/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

