//
//  SFConstants.h
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SFConstants : NSObject

extern NSString *const MyThingNotificationKey;
extern NSString *const FlickrAPIRoot;
extern NSString *const FlickrAPISearchMethod;
extern NSString *const FlickrAPIKey;

@end
