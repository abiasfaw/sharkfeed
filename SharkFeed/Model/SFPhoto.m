//
//  SFPhoto.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFPhoto.h"

@implementation SFPhoto

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if(dictionary) {
            _photoId = [dictionary objectForKey:@"id"];
            _ownerId = [dictionary objectForKey:@"owner"];
            _title = [dictionary objectForKey:@"title"];
            _thumbnailURL = [dictionary objectForKey:@"url_t"];
            _mediumSizeURL = [dictionary objectForKey:@"url_c"];
            _largeSizeURL = [dictionary objectForKey:@"url_l"];
            _originalSizeURL = [dictionary objectForKey:@"url_o"];
        }
    }
    
    return self;
}

- (NSString *)getLargeSizeOrSmallerImageURL {
    NSString *url = nil;
    
    if(_largeSizeURL) {
        url = _largeSizeURL;
    }
    else if(_mediumSizeURL) {
        url = _mediumSizeURL;
    }
    else if(_thumbnailURL) {
        url = _thumbnailURL;
    }
    
    return url;
}

@end
