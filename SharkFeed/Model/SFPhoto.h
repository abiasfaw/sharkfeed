//
//  SFPhoto.h
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  copy, readonlyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SFPhoto : NSObject

@property (nonatomic, copy, readonly) NSString* photoId;
@property (nonatomic, copy, readonly) NSString* ownerId;
@property (nonatomic, copy, readonly) NSString* title;
@property (nonatomic, copy, readonly) NSString* thumbnailURL;
@property (nonatomic, copy, readonly) NSString* mediumSizeURL;
@property (nonatomic, copy, readonly) NSString* largeSizeURL;
@property (nonatomic, copy, readonly) NSString* originalSizeURL;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (NSString *)getLargeSizeOrSmallerImageURL;

@end
