//
//  SFConstants.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/7/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFConstants.h"

@implementation SFConstants

NSString *const FlickrAPIRoot = @"https://api.flickr.com/services/rest/";
NSString *const FlickrAPISearchMethod = @"flickr.photos.search";
NSString *const FlickrAPIKey = @"949e98778755d1982f537d56236bbb42";

@end
