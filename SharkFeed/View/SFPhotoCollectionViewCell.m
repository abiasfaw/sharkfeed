//
//  SFPhotoCollectionViewCell.m
//  SharkFeed
//
//  Created by Muluken Gebremariam on 2/6/18.
//  Copyright © 2018 Muluken Gebremariam. All rights reserved.
//

#import "SFPhotoCollectionViewCell.h"

@implementation SFPhotoCollectionViewCell

+ (NSString *)identifier {
    return @"SFPhotoCollectionViewCell";
}

- (id)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]){
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        [self.contentView addSubview:_imageView];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if([super initWithCoder:aDecoder]) {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:_imageView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.imageView.image = nil;
    self.imageView.frame = self.contentView.bounds;
}

@end
